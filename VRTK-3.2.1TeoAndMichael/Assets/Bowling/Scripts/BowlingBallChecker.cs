﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
public class BowlingBallChecker : NetworkBehaviour {

    public ScoreScript ss;
    public pinScript[] ps;

    [SyncVar]
    public int pinsDown = 0;
    [SyncVar]
    public int turn = 0;

    [SerializeField]
    private int pointsFirstTry;
    [SerializeField]
    private int pointsBothTries;
    public int totalFrameScore;

    [SerializeField]
    private int totalScore = 0;

    private bool[] strike = new bool[9];


    void Awake()
    {
        ss = GameObject.Find("ScoreBoard").GetComponent<ScoreScript>();
        ps = FindObjectsOfType<pinScript>(); //this has to be changed later on if there are going to be 2 bowling alleys
    }
    void OnTriggerEnter(Collider col)
    {
        if (!isServer)
        {
            return;
        }

        if (col.gameObject.tag == "bowlingBall")
        {
            Debug.Log("bowlingBall Thrown");
            /*RpcStartCheckCoroutine(turn);*/
            StartCoroutine(CheckAndDisplayScore(turn));
            /*RpcStartCheckCoroutine();*/
            turn += 1;
        }
    }


   

    [ClientRpc]
    void RpcStartCheckCoroutine()
    {
        StartCoroutine(CheckAndDisplayScore(turn));
    }

    [ClientRpc]
    void RpcResetPins()
    {
        foreach (pinScript script in ps) // puts pins back up
        {
            script.resetPins();
        }
    }
    private IEnumerator CheckAndDisplayScore(int integer)
    {       

        yield return new WaitForSeconds(5f);
        switch (integer)
        {
            ///////////////////////////////////////////////////////////////////
            //////////////////////FIRST FRAME/////////////////////////////////
            //////////////////////////////////////////////////////////////////
            case 0:              
                if(pinsDown == 10) //if strike
                {
                    ss.firstShot = "X"; //display the strike mark
                    ss.P1ScoreList[2].text = 10.ToString();//display the total score
                    totalScore = 10;
                    turn += 1;//jumps to next frame
                    strike[0] = true;

                    RpcResetPins();
                }
                else 
                {
                    ss.firstShot = pinsDown.ToString(); //if didn't get them all                
                    pointsFirstTry = pinsDown; // pins down first shot  
                }
                break;

            case 1: //second try, first frame

                pointsBothTries = pinsDown; // pins down second try                           
                if (pointsBothTries == 10)
                {
                    ss.P1ScoreList[1].text = "/";
                    ss.P1ScoreList[2].text = pointsBothTries.ToString();
                    totalScore = 10;
                } 
                else {
                    ss.P1ScoreList[1].text = (pointsBothTries - pointsFirstTry).ToString();
                    ss.P1ScoreList[2].text = pointsBothTries.ToString();
                    totalScore = pointsBothTries;
                }

                RpcResetPins();
                break;

            ///////////////////////////////////////////////////////////////////
            //////////////////////Second FRAME/////////////////////////////////
            //////////////////////////////////////////////////////////////////
            case 2:
                if (pinsDown == 10) //if strike
                {
                    ss.P1ScoreList[3].text = "X"; //display the strike mark
                    totalScore += 10;
                    ss.P1ScoreList[5].text = totalScore.ToString();//display the total score    
                    strike[1] = true;
                    turn += 1;//jumps to next frame

                    RpcResetPins();
                }
                else if (strike[0])  //first try after a strike, it adds the points to the previous frame also
                {                                      
                    ss.P1ScoreList[3].text = pinsDown.ToString();//current shot taken
                    pointsFirstTry = pinsDown;//storing the first try
                    totalScore += pointsFirstTry*2;//adds the points to the total score / multiplied by two so it takes into account the points added to the previous frame as well as the points on this frame
                    ss.P1ScoreList[2].text = (int.Parse(ss.P1ScoreList[2].text) + pointsFirstTry).ToString();//adding the points to the previous frame
                }
                else
                {
                    ss.P1ScoreList[3].text = pinsDown.ToString(); //if didn't get them all
                    pointsFirstTry = pinsDown; // pins down first shot  
                }              
                break;

            case 3: //second try, second frame

                pointsBothTries = pinsDown; // pins down second try                           
                if (pointsBothTries == 10)
                {
                    ss.P1ScoreList[4].text = "/";
                    totalScore += 10;
                    ss.P1ScoreList[5].text = totalScore.ToString(); //this is the total                   
                }
                else if (strike[0])
                {
                    ss.P1ScoreList[4].text = (pointsBothTries - pointsFirstTry).ToString();//current shot taken             
                    ss.P1ScoreList[2].text = (int.Parse(ss.P1ScoreList[2].text) + pointsBothTries - pointsFirstTry).ToString();//adding the points of the second throw after a strike on the first frame
                    totalScore += (pointsBothTries-pointsFirstTry)*2;//adding the points to the totalScore variable
                    ss.P1ScoreList[5].text = totalScore.ToString();
                }

                else
                {
                    ss.P1ScoreList[4].text = (pointsBothTries - pointsFirstTry).ToString();
                    totalScore += pointsBothTries;
                    ss.P1ScoreList[5].text = totalScore.ToString();                   
                }

                RpcResetPins();
                break;

            ///////////////////////////////////////////////////////////////////
            //////////////////////THIRD FRAME/////////////////////////////////
            //////////////////////////////////////////////////////////////////
            case 4:
                if (pinsDown == 10) //if strike
                {
                    ss.P1ScoreList[6].text = "X"; //display the strike mark
                    totalScore += 10;
                    ss.P1ScoreList[8].text = totalScore.ToString();//display the total score
                    strike[2] = true;
                    turn += 1;//jumps to next frame

                    foreach (pinScript script in ps) // puts pins back up
                    {
                        script.resetPins();
                    }
                }
                else if (strike[1])  //first try after a strike, it adds the points to the previous frame also
                {
                    ss.P1ScoreList[6].text = pinsDown.ToString();//current shot taken
                    pointsFirstTry = pinsDown;//storing the first try
                    totalScore += pointsFirstTry * 2;//adds the points to the total score / multiplied by two so it takes into account the points added to the previous frame as well as the points on this frame
                    ss.P1ScoreList[5].text = (int.Parse(ss.P1ScoreList[5].text) + pointsFirstTry).ToString();//adding the points to the previous frame
                }
                else
                {
                    ss.P1ScoreList[6].text = pinsDown.ToString(); //if didn't get them all
                    pointsFirstTry = pinsDown; // pins down first shot  
                }
                break;

            case 5: //second try, third frame

                pointsBothTries = pinsDown; // pins down second try       
                 if (strike[1])
                {
                    ss.P1ScoreList[7].text = (pointsBothTries - pointsFirstTry).ToString();//current shot taken             
                    ss.P1ScoreList[5].text = (int.Parse(ss.P1ScoreList[5].text) + pointsBothTries - pointsFirstTry).ToString();//adding the points of the second throw after a strike on the previous frame
                    totalScore += (pointsBothTries - pointsFirstTry) * 2;//adding the points to the totalScore variable
                    ss.P1ScoreList[8].text = totalScore.ToString();//total of all frames but displayed on this frame
                }
                else if (pointsBothTries == 10)
                {
                    ss.P1ScoreList[7].text = "/";
                    totalScore += 10;
                    ss.P1ScoreList[8].text = totalScore.ToString(); //this is the total                  
                }
                else
                {
                    ss.P1ScoreList[7].text = (pointsBothTries - pointsFirstTry).ToString();
                    totalScore += pointsBothTries;
                    ss.P1ScoreList[8].text = totalScore.ToString();
               
                }

                foreach (pinScript script in ps) // puts pins back up
                {
                    script.resetPins();
                }
                break;

            ///////////////////////////////////////////////////////////////////
            //////////////////////FORTH FRAME/////////////////////////////////
            //////////////////////////////////////////////////////////////////
            case 6:
                if (pinsDown == 10) //if strike
                {
                    ss.P1ScoreList[9].text = "X"; //display the strike mark
                    totalScore += 10;
                    ss.P1ScoreList[11].text = totalScore.ToString();//display the total score
                    strike[3] = true;
                    turn += 1;//jumps to next frame

                    foreach (pinScript script in ps) // puts pins back up
                    {
                        script.resetPins();
                    }
                }
                else if (strike[2])  //first try after a strike, it adds the points to the previous frame also
                {
                    ss.P1ScoreList[9].text = pinsDown.ToString();//current shot taken
                    pointsFirstTry = pinsDown;//storing the first try
                    totalScore += pointsFirstTry * 2;//adds the points to the total score / multiplied by two so it takes into account the points added to the previous frame as well as the points on this frame
                    ss.P1ScoreList[8].text = (int.Parse(ss.P1ScoreList[8].text) + pointsFirstTry).ToString();//adding the points to the previous frame
                }
                else
                {
                    ss.P1ScoreList[9].text = pinsDown.ToString(); //if didn't get them all
                    pointsFirstTry = pinsDown; // pins down first shot  
                }
                break;

            case 7: //second try, forth frame

                pointsBothTries = pinsDown; // pins down second try         
                if (strike[2])
                {
                    ss.P1ScoreList[10].text = (pointsBothTries - pointsFirstTry).ToString();//second shot points      
                    ss.P1ScoreList[8].text = (int.Parse(ss.P1ScoreList[8].text) + pointsBothTries - pointsFirstTry).ToString();//adding the points of the second throw after a strike on the previous frame
                    totalScore += (pointsBothTries - pointsFirstTry) * 2;//adding the points of the second shot to the totalScore variable
                    ss.P1ScoreList[11].text = totalScore.ToString();//total of all frames but displayed on this frame
                }
                else if (pointsBothTries == 10)
                {
                    ss.P1ScoreList[10].text = "/";
                    totalScore += 10;
                    ss.P1ScoreList[11].text = totalScore.ToString(); //this is the total             
                } 
                else
                {
                    ss.P1ScoreList[10].text = (pointsBothTries - pointsFirstTry).ToString();
                    totalScore += pointsBothTries;
                    ss.P1ScoreList[11].text = totalScore.ToString();
                   
                }

                foreach (pinScript script in ps) // puts pins back up
                {
                    script.resetPins();
                }
                break;

            ///////////////////////////////////////////////////////////////////
            //////////////////////FIFTH FRAME/////////////////////////////////
            //////////////////////////////////////////////////////////////////
            case 8:
                if (pinsDown == 10) //if strike
                {
                    ss.P1ScoreList[12].text = "X"; //display the strike mark
                    totalScore += 10;
                    ss.P1ScoreList[14].text = totalScore.ToString();//display the total score
                    strike[4] = true;       
                    turn += 1;//jumps to next frame

                    foreach (pinScript script in ps) // puts pins back up
                    {
                        script.resetPins();
                    }
                }
                else if (strike[3])  //first try after a strike, it adds the points to the previous frame also 
                {
                    ss.P1ScoreList[12].text = pinsDown.ToString();//current shot taken
                    pointsFirstTry = pinsDown;//storing the first try
                    totalScore += pointsFirstTry * 2;//adds the points to the total score / multiplied by two so it takes into account the points added to the previous frame as well as the points on this frame
                    ss.P1ScoreList[11].text = (int.Parse(ss.P1ScoreList[11].text) + pointsFirstTry).ToString();//adding the points to the previous frame
                }
                else
                {
                    ss.P1ScoreList[12].text = pinsDown.ToString(); //if didn't get them all
                    pointsFirstTry = pinsDown; // pins down first shot  
                }
                break;

            case 9: //second try, fifth frame

                pointsBothTries = pinsDown; // pins down second try     
                if (strike[3])
                {
                    ss.P1ScoreList[13].text = (pointsBothTries - pointsFirstTry).ToString();//second shot points      
                    ss.P1ScoreList[11].text = (int.Parse(ss.P1ScoreList[11].text) + pointsBothTries - pointsFirstTry).ToString();//adding the points of the second throw after a strike on the previous frame
                    totalScore += (pointsBothTries - pointsFirstTry) * 2;//adding the points of the second shot to the totalScore variable
                    ss.P1ScoreList[14].text = totalScore.ToString();//total of all frames but displayed on this frame
                }
                else if (pointsBothTries == 10)
                {
                    ss.P1ScoreList[13].text = "/";
                    totalScore += 10;
                    ss.P1ScoreList[14].text = totalScore.ToString(); //this is the total 

                } 
                else
                {
                    ss.P1ScoreList[13].text = (pointsBothTries - pointsFirstTry).ToString();
                    totalScore += pointsBothTries;
                    ss.P1ScoreList[14].text = totalScore.ToString();
                }

                foreach (pinScript script in ps) // puts pins back up
                {
                    script.resetPins();
                }
                break;

            ///////////////////////////////////////////////////////////////////
            //////////////////////SIXTH FRAME/////////////////////////////////
            //////////////////////////////////////////////////////////////////
            case 10:
                if (pinsDown == 10) //if strike
                {
                    ss.P1ScoreList[15].text = "X"; //display the strike mark
                    totalScore += 10;
                    ss.P1ScoreList[17].text = totalScore.ToString();//display the total score
                    strike[5] = true;
                    turn += 1;//jumps to next frame

                    foreach (pinScript script in ps) // puts pins back up
                    {
                        script.resetPins();
                    }
                }
                else if (strike[4])  //first try after a strike, it adds the points to the previous frame also 
                {
                    ss.P1ScoreList[15].text = pinsDown.ToString();//current shot taken
                    pointsFirstTry = pinsDown;//storing the first try
                    totalScore += pointsFirstTry * 2;//adds the points to the total score / multiplied by two so it takes into account the points added to the previous frame as well as the points on this frame
                    ss.P1ScoreList[14].text = (int.Parse(ss.P1ScoreList[14].text) + pointsFirstTry).ToString();//adding the points to the previous frame
                }
                else
                {
                    ss.P1ScoreList[15].text = pinsDown.ToString(); //if didn't get them all
                    pointsFirstTry = pinsDown; // pins down first shot  
                }
                break;

            case 11: //second try, sixth frame 15 16 17

                pointsBothTries = pinsDown; // pins down second try    
                if (strike[4])
                {
                    ss.P1ScoreList[16].text = (pointsBothTries - pointsFirstTry).ToString();//second shot points      
                    ss.P1ScoreList[14].text = (int.Parse(ss.P1ScoreList[14].text) + pointsBothTries - pointsFirstTry).ToString();//adding the points of the second throw after a strike on the previous frame
                    totalScore += (pointsBothTries - pointsFirstTry) * 2;//adding the points of the second shot to the totalScore variable
                    ss.P1ScoreList[17].text = totalScore.ToString();//total of all frames but displayed on this frame
                }
                else if (pointsBothTries == 10)
                {
                    ss.P1ScoreList[16].text = "/";
                    totalScore += 10;
                    ss.P1ScoreList[17].text = totalScore.ToString(); //this is the total 
                } 
                else
                {
                    ss.P1ScoreList[16].text = (pointsBothTries - pointsFirstTry).ToString();
                    totalScore += pointsBothTries;
                    ss.P1ScoreList[17].text = totalScore.ToString();
                }
                foreach (pinScript script in ps) // puts pins back up
                {
                    script.resetPins();
                }
                break;

            ///////////////////////////////////////////////////////////////////
            //////////////////////SEVENTH FRAME/////////////////////////////////
            //////////////////////////////////////////////////////////////////
            case 12:
                if (pinsDown == 10) //if strike
                {
                    ss.P1ScoreList[18].text = "X"; //display the strike mark
                    totalScore += 10;
                    ss.P1ScoreList[18].text = totalScore.ToString();//display the total score
                    strike[6] = true;
                    turn += 1;//jumps to next frame

                    foreach (pinScript script in ps) // puts pins back up
                    {
                        script.resetPins();
                    }
                }
                else if (strike[5])  //first try after a strike, it adds the points to the previous frame also 
                {
                    ss.P1ScoreList[18].text = pinsDown.ToString();//current shot taken
                    pointsFirstTry = pinsDown;//storing the first try
                    totalScore += pointsFirstTry * 2;//adds the points to the total score / multiplied by two so it takes into account the points added to the previous frame as well as the points on this frame
                    ss.P1ScoreList[17].text = (int.Parse(ss.P1ScoreList[17].text) + pointsFirstTry).ToString();//adding the points to the previous frame
                }
                else
                {
                    ss.P1ScoreList[18].text = pinsDown.ToString(); //if didn't get them all
                    pointsFirstTry = pinsDown; // pins down first shot  
                }
                break;

            case 13: //second try, seventh frame

                pointsBothTries = pinsDown; // pins down second try      
                if (strike[5])
                {
                    ss.P1ScoreList[19].text = (pointsBothTries - pointsFirstTry).ToString();//second shot points      
                    ss.P1ScoreList[18].text = (int.Parse(ss.P1ScoreList[18].text) + pointsBothTries - pointsFirstTry).ToString();//adding the points of the second throw after a strike on the previous frame
                    totalScore += (pointsBothTries - pointsFirstTry) * 2;//adding the points of the second shot to the totalScore variable
                    ss.P1ScoreList[20].text = totalScore.ToString();//total of all frames but displayed on this frame
                }
                else if (pointsBothTries == 10)
                {
                    ss.P1ScoreList[19].text = "/";
                    totalScore += 10;
                    ss.P1ScoreList[20].text = totalScore.ToString(); //this is the total 
                } 
                else
                {
                    ss.P1ScoreList[19].text = (pointsBothTries - pointsFirstTry).ToString();
                    totalScore += pointsBothTries;
                    ss.P1ScoreList[20].text = totalScore.ToString();
                }
                foreach (pinScript script in ps) // puts pins back up
                {
                    script.resetPins();
                }
                break;

            ///////////////////////////////////////////////////////////////////
            //////////////////////EIGHT FRAME/////////////////////////////////
            //////////////////////////////////////////////////////////////////
            case 14:
                if (pinsDown == 10) //if strike
                {
                    ss.P1ScoreList[21].text = "X"; //display the strike mark
                    totalScore += 10;
                    ss.P1ScoreList[23].text = totalScore.ToString();//display the total score
                    strike[7] = true;
                    turn += 1;//jumps to next frame

                    foreach (pinScript script in ps) // puts pins back up
                    {
                        script.resetPins();
                    }
                }
                else if (strike[6])  // first try after a strike, it adds the points to the previous frame also 
                {
                    ss.P1ScoreList[21].text = pinsDown.ToString();//current shot taken
                    pointsFirstTry = pinsDown;//storing the first try
                    totalScore += pointsFirstTry * 2;//adds the points to the total score / multiplied by two so it takes into account the points added to the previous frame as well as the points on this frame
                    ss.P1ScoreList[20].text = (int.Parse(ss.P1ScoreList[20].text) + pointsFirstTry).ToString();//adding the points to the previous frame
                }
                else
                {
                    ss.P1ScoreList[21].text = pinsDown.ToString(); //if didn't get them all
                    pointsFirstTry = pinsDown; // pins down first shot  
                }
                break;

            case 15: //second eighth, first frame

                pointsBothTries = pinsDown; // pins down second try          
                if (strike[6])
                {
                    ss.P1ScoreList[22].text = (pointsBothTries - pointsFirstTry).ToString();//second shot points      
                    ss.P1ScoreList[20].text = (int.Parse(ss.P1ScoreList[20].text) + pointsBothTries - pointsFirstTry).ToString();//adding the points of the second throw after a strike on the previous frame
                    totalScore += (pointsBothTries - pointsFirstTry) * 2;//adding the points of the second shot to the totalScore variable
                    ss.P1ScoreList[23].text = totalScore.ToString();//total of all frames but displayed on this frame
                }
                else if (pointsBothTries == 10)
                {
                    ss.P1ScoreList[22].text = "/";
                    totalScore += 10;
                    ss.P1ScoreList[23].text = totalScore.ToString(); //this is the total 
                } 
                else
                {
                    ss.P1ScoreList[22].text = (pointsBothTries - pointsFirstTry).ToString();
                    totalScore += pointsBothTries;
                    ss.P1ScoreList[23].text = totalScore.ToString();
                }
                foreach (pinScript script in ps) // puts pins back up
                {
                    script.resetPins();
                }
                break;

            ///////////////////////////////////////////////////////////////////
            //////////////////////Nineth FRAME/////////////////////////////////
            //////////////////////////////////////////////////////////////////
            case 16:
                if (pinsDown == 10) //if strike
                {
                    ss.P1ScoreList[24].text = "X"; //display the strike mark
                    totalScore += 10;
                    ss.P1ScoreList[26].text = totalScore.ToString();//display the total score
                    strike[8] = true;
                    turn += 1;//jumps to next frame

                    foreach (pinScript script in ps) // puts pins back up
                    {
                        script.resetPins();
                    }
                }
                else if (strike[7])  //first try after a strike, it adds the points to the previous frame also 
                {
                    ss.P1ScoreList[24].text = pinsDown.ToString();//current shot taken
                    pointsFirstTry = pinsDown;//storing the first try
                    totalScore += pointsFirstTry * 2;//adds the points to the total score / multiplied by two so it takes into account the points added to the previous frame as well as the points on this frame
                    ss.P1ScoreList[23].text = (int.Parse(ss.P1ScoreList[23].text) + pointsFirstTry).ToString();//adding the points to the previous frame
                }
                else
                {
                    ss.P1ScoreList[24].text = pinsDown.ToString(); //if didn't get them all
                    pointsFirstTry = pinsDown; // pins down first shot  
                }
                break;

            case 17: //second try, nineth frame

                pointsBothTries = pinsDown; // pins down second try      
                if (strike[7])
                {
                    ss.P1ScoreList[25].text = (pointsBothTries - pointsFirstTry).ToString();//second shot points      
                    ss.P1ScoreList[23].text = (int.Parse(ss.P1ScoreList[23].text) + pointsBothTries - pointsFirstTry).ToString();//adding the points of the second throw after a strike on the previous frame
                    totalScore += (pointsBothTries - pointsFirstTry) * 2;//adding the points of the second shot to the totalScore variable
                    ss.P1ScoreList[26].text = totalScore.ToString();//total of all frames but displayed on this frame
                }
                else if (pointsBothTries == 10)
                {
                    ss.P1ScoreList[25].text = "/";
                    totalScore += 10;
                    ss.P1ScoreList[26].text = totalScore.ToString(); //this is the total 
                } 
                else
                {
                    ss.P1ScoreList[25].text = (pointsBothTries - pointsFirstTry).ToString();
                    totalScore += pointsBothTries;
                    ss.P1ScoreList[26].text = totalScore.ToString();
                }
                foreach (pinScript script in ps) // puts pins back up
                {
                    script.resetPins();
                }
                break;

            ///////////////////////////////////////////////////////////////////
            //////////////////////LAST FRAME/////////////////////////////////
            //////////////////////////////////////////////////////////////////
            case 18:
                if (pinsDown == 10) //if strike
                {
                    ss.P1ScoreList[27].text = "X"; //display the strike mark
                    totalScore += 10;
                    ss.P1ScoreList[29].text = totalScore.ToString();//display the total score
                    strike[9] = true;                  
                    turn += 1;//jumps to next frame

                    foreach (pinScript script in ps) // puts pins back up
                    {
                        script.resetPins();
                    }
                }
                else if (strike[8])  //first try after a strike, it adds the points to the previous frame also 
                {
                    ss.P1ScoreList[27].text = pinsDown.ToString();//current shot taken
                    pointsFirstTry = pinsDown;//storing the first try
                    totalScore += pointsFirstTry * 2;//adds the points to the total score / multiplied by two so it takes into account the points added to the previous frame as well as the points on this frame
                    ss.P1ScoreList[26].text = (int.Parse(ss.P1ScoreList[26].text) + pointsFirstTry).ToString();//adding the points to the previous frame
                }
                else
                {
                    ss.P1ScoreList[27].text = pinsDown.ToString(); //if didn't get them all
                    pointsFirstTry = pinsDown; // pins down first shot  
                }
                break;

            case 19: //second try, last frame

                pointsBothTries = pinsDown; // pins down second try         
                if (strike[8])
                {
                    ss.P1ScoreList[28].text = (pointsBothTries - pointsFirstTry).ToString();//second shot points      
                    ss.P1ScoreList[26].text = (int.Parse(ss.P1ScoreList[26].text) + pointsBothTries - pointsFirstTry).ToString();//adding the points of the second throw after a strike on the previous frame
                    totalScore += (pointsBothTries - pointsFirstTry) * 2;//adding the points of the second shot to the totalScore variable
                    ss.P1ScoreList[29].text = totalScore.ToString();//total of all frames but displayed on this frame
                }
                else if (pointsBothTries == 10)
                {
                    ss.P1ScoreList[28].text = "/";
                    totalScore += 10;
                    ss.P1ScoreList[29].text = totalScore.ToString(); //this is the total 
                } 
                else
                {
                    ss.P1ScoreList[28].text = (pointsBothTries - pointsFirstTry).ToString();
                    totalScore += pointsBothTries;
                    ss.P1ScoreList[29].text = totalScore.ToString();
                }
                foreach (pinScript script in ps) // puts pins back up
                {
                    script.resetPins();
                }
                break;
        }
    }
}
