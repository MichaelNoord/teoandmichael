﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BowlingScoreDisplayer : MonoBehaviour {

    public int pinsDown = 0;
    public Text scoreText;
    
void Awake()
    {
        scoreText = GetComponentInChildren<Text>();
    }

    void Update()
    {
        scoreText.text = pinsDown.ToString()+ "/10 Pins Down";  

        if(pinsDown == 10)
        {
            scoreText.text = "Strike!";
        }
    }
}
