﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pinScript : MonoBehaviour {

    public bool isPinDown;
    public BowlingBallChecker score;
    private Vector3 originalPinPosition;
    private Quaternion originalPinRotation;


    void Awake()
    {
        score = GameObject.Find("BowlingBallChecker").GetComponent<BowlingBallChecker>();
        originalPinPosition = transform.position;
        originalPinRotation = transform.rotation;
    }

  
    void OnCollisionEnter(Collision col)
    {
        if(col.gameObject.tag == "bowlingBall" || col.gameObject.tag == "pin")
        {
            StartCoroutine(checkPinsDown());
        }
    }

    public void resetPins()
    {
        isPinDown = false;
        transform.position = originalPinPosition;
        transform.rotation = originalPinRotation;
        score.pinsDown = 0;
    }
    IEnumerator checkPinsDown()
    {
        yield return new WaitForSeconds(1f);

             if (!isPinDown)
        {
            if (this.gameObject.transform.up.y <= 0.8f)
            {
                score.pinsDown += 1;
                isPinDown = true;
                //should move the pins below so you dont see them anymore in case there wasn't a strike (after a couple of seconds)
                yield return new WaitForSeconds(2f);
                transform.position = new Vector3 (transform.position.x, -1f, transform.position.z);
            }
        }
    }

    
}
