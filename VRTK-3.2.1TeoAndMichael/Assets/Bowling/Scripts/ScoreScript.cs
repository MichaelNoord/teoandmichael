﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class ScoreScript : NetworkBehaviour
{
    public Text[] P1ScoreList;

    [SerializeField]
    public Text[] P2ScoreList;

    [SyncVar]
    public string firstShot;

    void Awake()
    {
        P1ScoreList = GameObject.Find("P1_Score").GetComponentsInChildren<Text>();
        P2ScoreList = GameObject.Find("P2_Score").GetComponentsInChildren<Text>();
        firstShot = P1ScoreList[0].text;
    } 

   void Update()
    {
        Debug.Log(firstShot + "prima lovitura");
    }
}
