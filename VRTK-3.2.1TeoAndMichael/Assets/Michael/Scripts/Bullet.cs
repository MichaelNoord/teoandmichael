﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public int SetTag = 0;
    // Use this for initialization
    void Start()
    {
        SetTagMethod();
    }

    // Update is called once per frame

    void SetTagMethod()
    {
        SetTag = Random.Range(0, 3);
        switch (SetTag)
        {
            case 0:
                gameObject.tag = "WeakBullet";
                break;
            case 1:
                gameObject.tag = "MedBullet";
                break;
            case 2:
                gameObject.tag = "StrongBullet";
                break;

            default:
                break;
        }
    }
}
