﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnObject : MonoBehaviour {

    public Vector3 Center;
    public Vector3 Size;

    public GameObject PolePrefab;
    public GameObject thePole;
	// Use this for initialization
	void Start () 
    {
	}
	
	// Update is called once per frame

    void OnDrawGizmosSelected() 
    {
        Gizmos.color = new Color(1, 0, 0, 0.5f);
        Gizmos.DrawCube(transform.localPosition + Center, Size); 
    }
}
