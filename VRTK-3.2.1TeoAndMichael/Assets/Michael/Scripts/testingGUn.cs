﻿namespace VRTK.Examples
{
    using UnityEngine;
    using VRTK;

    public class testingGUn : VRTK_InteractableObject
    {
        public GameObject bullet;
        private float bulletSpeed;
        private float bulletLife = 3;

        // Use this for initialization
        public override void StartUsing(VRTK_InteractUse usingObject)
        {
            base.StartUsing(usingObject);
            FireBullet();
            bulletSpeed = Random.Range(1000, 2000);
        }

        protected void Start()
        {
            bullet = transform.Find("Bullet").gameObject;
            bullet.SetActive(false);
         
        }

      
          
      
        private void FireBullet()
        {
            GameObject bulletClone = Instantiate(bullet, bullet.transform.position, bullet.transform.rotation) as GameObject;
            bulletClone.SetActive(true);
            Rigidbody rb = bulletClone.GetComponent<Rigidbody>();
            rb.AddForce(-bullet.transform.forward * bulletSpeed);
            Destroy(bulletClone, bulletLife);
        }
    }
}
