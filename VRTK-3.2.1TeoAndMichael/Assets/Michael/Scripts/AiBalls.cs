﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class AiBalls : MonoBehaviour
{
    public Vector3 PoleVector;
    public float health;
    public Material Red;
    public Material Orange;
    public Material Green;
    public GameObject Pole;
    public float Speed;
    public float SpeedFast;
    // Use this for initialization
    void Start()
    {
        SpeedFast = Random.Range(0.0f, 0.1f);
        health = Random.Range(50, 150);
        SetColor();

      
    }

    // Update is called once per frame
    void Update()
    {
        SetColor();
        AttackPole();

        PoleVector.x = Pole.transform.position.x;
        PoleVector.y = Pole.transform.position.y;
        PoleVector.z = Pole.transform.position.z;
    }
    void AttackPole()
    {
        Speed = SpeedFast - Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, PoleVector, Speed);
        if (Speed <= 0)
        {
            Speed = 0.01f;
        }
    }

    void SetColor()
    {
        if (health >= 100)
        {
            GetComponent<Renderer>().material = Green;
        }

        if (health <= 100)
        {
            GetComponent<Renderer>().material = Orange;
        }

        if (health <= 50)
        {
            GetComponent<Renderer>().material = Red;
        }
    }

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "WeakBullet")
        {
            health -= 25;
        }
        if (collision.gameObject.tag == "MedBullet")
        {
            health -= 50;
        }
        if (collision.gameObject.tag == "StrongBullet")
        {
            health -= 75;
        }

        death();
    }


    void death()
    {
        if (health <= 0)
        {
            Destroy(gameObject);
        }
    }
}
