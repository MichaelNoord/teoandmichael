﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BowlingBall : MonoBehaviour {

    public Rigidbody rb;

	// Use this for initialization
	void Start () 
    {
        rb = GetComponent<Rigidbody>();
        rb.maxAngularVelocity = 40;
        rb.angularDrag = 0.05f;

    }
	
	// Update is called once per frame
	void Update () {
		
	}


}
