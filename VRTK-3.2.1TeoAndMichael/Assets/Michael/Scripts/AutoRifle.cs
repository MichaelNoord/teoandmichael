﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Valve.VR;
using VRTK;
using UnityEngine.Networking;


public class AutoRifle : VRTK_InteractableObject
{
    public GameObject bullet;
    private float bulletSpeed;
    private float bulletLife = 3;
    private int Bullets = 30;


    public VRTK_ControllerEvents RightController;
    private float TimeInbetweenShots = 0;
    private float MaxTimeInBetweenShots = 0.06f;
    public bool IsFiring = false;


    // Use this for initialization
    public override void StartUsing(VRTK_InteractUse usingObject)
    {
        base.StartUsing(usingObject);
        FireBullet();
        bulletSpeed = UnityEngine.Random.Range(1000, 2000);
        IsFiring = true;
    }

    public void Start()
    {
        bullet = transform.Find("Bullet").gameObject;
        bullet.SetActive(false);
        RightController = FindObjectOfType<VRTK_ControllerEvents>();

    }
    public void Update()
    {
        if (RightController.triggerPressed)
        {
            FireBullet();
        }

        if (RightController.buttonOnePressed)
        {
            ReloadGun();
        }
    }

    private void ReloadGun()
    {
        Bullets = 30;
    }

    public void FireBullet()
    {
        if (Bullets >= 0)
        {
            TimeInbetweenShots += Time.deltaTime;
            if (TimeInbetweenShots >= MaxTimeInBetweenShots)
            {
                Bullets--;
                
                
                GameObject bulletClone = Instantiate(bullet, bullet.transform.position, bullet.transform.rotation) as GameObject;
                bulletClone.SetActive(true);
                Rigidbody rb = bulletClone.GetComponent<Rigidbody>();
                rb.AddForce(-bullet.transform.forward * bulletSpeed);
                NetworkServer.Spawn(bullet);
                Destroy(bulletClone, bulletLife);
                TimeInbetweenShots = 0;
            }
        }
    }
}
