﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

[DisallowMultipleComponent]
[RequireComponent(typeof(Rigidbody))]
public class Actor : NetworkBehaviour
{
    #region public

    #endregion
    #region protected
    // Variables
    #endregion
    #region private
    // Variables

    #endregion

    /// <summary>
    /// Starts this instance.
    /// </summary>
    protected virtual void Start()
    {

    }

    /// <summary>
    /// Updates this instance.
    /// </summary>
    protected virtual void Update()
    {
        /**/
    }
}