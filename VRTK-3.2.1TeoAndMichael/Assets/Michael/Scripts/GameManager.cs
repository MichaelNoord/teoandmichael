﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using VRTK;

public class GameManager : MonoBehaviour
{
    #region public
    // Classes
    public static GameManager instance;

    // Objects
    public GameObject localPlayer;
    public GameObject VRTKSDKManager;

    // Collections
    public Dictionary<int, GameObject> players;
    public List<int> playerNetIds;

    // Variables
    public bool networkStarted;

    #endregion
    #region private
    // Variables
    #endregion

    public GameManager()
    {
        instance = this;
    }

    private void Start()
    {
        players = new Dictionary<int, GameObject>();
    }
}