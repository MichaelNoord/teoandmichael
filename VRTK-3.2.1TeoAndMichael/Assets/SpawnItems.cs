﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class SpawnItems : NetworkBehaviour
{
    public GameObject gunPrefab;
    public Transform gunSpawn;

    public bool gunSpawned = false;
    public bool ScriptSpawned = false;

    public override void OnStartLocalPlayer()
    {
        GetComponent<MeshRenderer>().material.color = Color.blue;
    }

    void Start()
    {
        CmdFire();

        if (!isLocalPlayer)
        {
            return;
        }
    }


    void CmdFire()
    {
        if (gunSpawned == false)
        {

            var Weapon = (GameObject)Instantiate(
                gunPrefab,
                gunSpawn.position,
                gunSpawn.rotation);
            NetworkServer.Spawn(Weapon);
            gunSpawned = true;
        }

        if (ScriptSpawned == false)
        {
           /* var VRTK_scripts = (GameObject)Instantiate(
                  VRTK_SCRIPTS,
                  gunSpawn.position,
                  gunSpawn.rotation);
            NetworkServer.Spawn(VRTK_scripts);
            gunSpawned = true;*/
        }
    }
}
