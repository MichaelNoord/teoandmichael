﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions.Must;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using VRTK;

public class PlayerSetup : Actor
{

    public PlayerSetup localPlayer;
    public bool networkInitialized;
    private bool notFirstTimeMain;

    public Behaviour[] componentsToDisable;
    public GameObject[] gameObjectsToDisable;

    protected override void Start()
    {
        if (isLocalPlayer)
        {
            localPlayer = this;
        }

    }
    protected override void Update()
    {
        base.Update();

    }

    private void FixedUpdate()
    {

        if (!GameManager.instance)
        {
            return;
        }

        if (!GameManager.instance.VRTKSDKManager)
        {
            GameManager.instance.VRTKSDKManager = GameObject.Find("[VRTK_SDKManager]");
        }
        if (isLocalPlayer)
        {
            GameManager.instance.localPlayer = gameObject;
        }

      /*  
        if (isLocalPlayer && GameManager.instance.VRTKSDKManager.transform.Find("SDKSetups").transform.Find("Simulator").transform.Find("VRSimulatorCameraRig").transform.Find("Neck").transform.Find("Camera"))
        {
            transform.position = GameManager.instance.VRTKSDKManager.transform.position;
            transform.rotation = GameManager.instance.VRTKSDKManager.transform.rotation;

            transform.Find("Simulator").transform.Find("VRSimulatorCameraRig").transform.localPosition = GameManager.instance.VRTKSDKManager.transform.Find("SDKSetups").transform.Find("Simulator").transform.Find("VRSimulatorCameraRig").transform.localPosition;
            transform.Find("Simulator").transform.Find("VRSimulatorCameraRig").transform.localRotation = GameManager.instance.VRTKSDKManager.transform.Find("SDKSetups").transform.Find("Simulator").transform.Find("VRSimulatorCameraRig").transform.localRotation;
            transform.Find("Simulator").transform.Find("VRSimulatorCameraRig").transform.Find("LeftHand").transform.Find("Hand").transform.localPosition = GameManager.instance.VRTKSDKManager.transform.Find("SDKSetups").transform.Find("Simulator").transform.Find("VRSimulatorCameraRig").transform.Find("LeftHand").transform.Find("Hand").transform.localPosition;
            transform.Find("Simulator").transform.Find("VRSimulatorCameraRig").transform.Find("LeftHand").transform.Find("Hand").transform.localRotation = GameManager.instance.VRTKSDKManager.transform.Find("SDKSetups").transform.Find("Simulator").transform.Find("VRSimulatorCameraRig").transform.Find("LeftHand").transform.Find("Hand").transform.localRotation;
            transform.Find("Simulator").transform.Find("VRSimulatorCameraRig").transform.Find("RightHand").transform.Find("Hand").transform.localPosition = GameManager.instance.VRTKSDKManager.transform.Find("SDKSetups").transform.Find("Simulator").transform.Find("VRSimulatorCameraRig").transform.Find("RightHand").transform.Find("Hand").transform.localPosition;
            transform.Find("Simulator").transform.Find("VRSimulatorCameraRig").transform.Find("RightHand").transform.Find("Hand").transform.localRotation = GameManager.instance.VRTKSDKManager.transform.Find("SDKSetups").transform.Find("Simulator").transform.Find("VRSimulatorCameraRig").transform.Find("RightHand").transform.Find("Hand").transform.localRotation;
            transform.Find("Simulator").transform.Find("VRSimulatorCameraRig").transform.Find("Neck").transform.Find("Camera").transform.localPosition = GameManager.instance.VRTKSDKManager.transform.Find("SDKSetups").transform.Find("Simulator").transform.Find("VRSimulatorCameraRig").transform.Find("Neck").transform.Find("Camera").transform.localPosition;
            transform.Find("Simulator").transform.Find("VRSimulatorCameraRig").transform.Find("Neck").transform.Find("Camera").transform.localRotation = GameManager.instance.VRTKSDKManager.transform.Find("SDKSetups").transform.Find("Simulator").transform.Find("VRSimulatorCameraRig").transform.Find("Neck").transform.Find("Camera").transform.localRotation;
        }*/

        // SteamVR
        if (isLocalPlayer && GameManager.instance.VRTKSDKManager.transform.Find("SDKSetups").transform.Find("SteamVR").transform.Find("[CameraRig]").transform.Find("Camera (eye)"))
        {
            transform.position = GameManager.instance.VRTKSDKManager.transform.position;
            transform.rotation = GameManager.instance.VRTKSDKManager.transform.rotation;

            transform.Find("SteamVR").transform.Find("[CameraRig]").transform.localPosition = GameManager.instance.VRTKSDKManager.transform.Find("SDKSetups").transform.Find("SteamVR").transform.Find("[CameraRig]").transform.localPosition;
            transform.Find("SteamVR").transform.Find("[CameraRig]").transform.localRotation = GameManager.instance.VRTKSDKManager.transform.Find("SDKSetups").transform.Find("SteamVR").transform.Find("[CameraRig]").transform.localRotation;
            transform.Find("SteamVR").transform.Find("[CameraRig]").transform.Find("Controller (left)").transform.localPosition = GameManager.instance.VRTKSDKManager.transform.Find("SDKSetups").transform.Find("SteamVR").transform.Find("[CameraRig]").transform.Find("Controller (left)").transform.localPosition;
            transform.Find("SteamVR").transform.Find("[CameraRig]").transform.Find("Controller (left)").transform.localRotation = GameManager.instance.VRTKSDKManager.transform.Find("SDKSetups").transform.Find("SteamVR").transform.Find("[CameraRig]").transform.Find("Controller (left)").transform.localRotation;
            transform.Find("SteamVR").transform.Find("[CameraRig]").transform.Find("Controller (right)").transform.localPosition = GameManager.instance.VRTKSDKManager.transform.Find("SDKSetups").transform.Find("SteamVR").transform.Find("[CameraRig]").transform.Find("Controller (right)").transform.localPosition;
            transform.Find("SteamVR").transform.Find("[CameraRig]").transform.Find("Controller (right)").transform.localRotation = GameManager.instance.VRTKSDKManager.transform.Find("SDKSetups").transform.Find("SteamVR").transform.Find("[CameraRig]").transform.Find("Controller (right)").transform.localRotation;
            //transform.Find("SteamVR").transform.Find("[CameraRig]").transform.Find("Camera (ears)").transform.localPosition = GameManager.instance.VRTKSDKManager.transform.Find("SDKSetups").transform.Find("SteamVR").transform.Find("[CameraRig]").transform.Find("Camera (eye)").transform.localPosition;
            //transform.Find("SteamVR").transform.Find("[CameraRig]").transform.Find("Camera (ears)").transform.localRotation = GameManager.instance.VRTKSDKManager.transform.Find("SDKSetups").transform.Find("SteamVR").transform.Find("[CameraRig]").transform.Find("Camera (eye)").transform.localRotation;

            Debug.Log("SteamVR also works in here :)");
        }
        
        if (isLocalPlayer && !notFirstTimeMain && ClientScene.ready && SceneManager.GetActiveScene().name == "MichaelBowling")
        {
            /*if (GameManager.instance.VRTKSDKManager.transform.Find("SDKSetups").transform.Find("SteamVR").transform.Find("[CameraRig]").transform.Find("Controller (right)").transform.Find("RightController") != null)
            {

            }*/
            CmdPlayerConnected("", "");
            notFirstTimeMain = true;
        }
        if (isLocalPlayer && !notFirstTimeMain && ClientScene.ready && SceneManager.GetActiveScene().name == "SteamVRLobby")
        {
            /*if (GameManager.instance.VRTKSDKManager.transform.Find("SDKSetups").transform.Find("SteamVR").transform.Find("[CameraRig]").transform.Find("Controller (right)").transform.Find("RightController") != null)
            {

            }*/
            CmdPlayerConnected("", "");
            notFirstTimeMain = true;
        }

        if (isLocalPlayer && !notFirstTimeMain && ClientScene.ready && SceneManager.GetActiveScene().name == "MichaelScene")
        {
            /*if (GameManager.instance.VRTKSDKManager.transform.Find("SDKSetups").transform.Find("SteamVR").transform.Find("[CameraRig]").transform.Find("Controller (right)").transform.Find("RightController") != null)
            {

            }*/
            CmdPlayerConnected("", "");
            notFirstTimeMain = true;
        }
    }

    [Command]
    private void CmdPlayerConnected(string playerName, string playerCharacter)
    {
        //GameManager.instance.playerNames.Add((int)netId.Value, playerName);
        //GameManager.instance.playerCharacters.Add((int)netId.Value, playerCharacter);
        RpcPlayerConnected(playerName, playerCharacter);
        if (!GameManager.instance.playerNetIds.Contains((int)netId.Value))
        {
            GameManager.instance.playerNetIds.Add((int)netId.Value);
            //Add this player to the Gamemanager libary of players
            GameManager.instance.players.Add((int)netId.Value, gameObject);
        }
        for (int i = 0; i < GameManager.instance.playerNetIds.Count; i++)
        {
            //this should prevent it from firing for players who have already left
            if (GameManager.instance.players[GameManager.instance.playerNetIds[i]])
            {
                //playerName = GameManager.instance.playerNames[GameManager.instance.playerNetIds[i]];
                //playerCharacter = GameManager.instance.playerCharacters[GameManager.instance.playerNetIds[i]];
                GameManager.instance.players[GameManager.instance.playerNetIds[i]].GetComponent<PlayerSetup>().RpcPlayerConnected(playerName, playerCharacter);
            }
        }
        // Setup(playerName, playerCharacter);
        DontDestroyOnLoad(gameObject);

        GameManager.instance.networkStarted = true;
    }

    [ClientRpc]
    private void RpcPlayerConnected(string playerName, string playerCharacter)
    {
        if (!networkInitialized)
        {
            networkInitialized = true;

            // -Run the playersetup
            Setup(playerName, playerCharacter);

            DontDestroyOnLoad(gameObject);

            //CmdChangeScene(2);

            // -Assign methods to delegates from the tracked vr controllers
            if (isLocalPlayer)
            {
                //leftController.TriggerClicked += StartPickUp;
                //leftController.TriggerUnclicked += StopPickUp;

                //rightController.TriggerClicked += TelePort;
                //rightController.PadClicked += CreateIdenticator;

                /*GameManager.instance.VRTKSDKManager.transform.Find("SDKSetups").transform.Find("[CameraRig]").transform.Find("Controller (left)").GetComponent<VRTK_InteractGrab>().GrabButtonPressed += StartGrabAnimation;
                GameManager.instance.VRTKSDKManager.transform.Find("SDKSetups").transform.Find("[CameraRig]").transform.Find("Controller (left)").GetComponent<VRTK_InteractGrab>().GrabButtonReleased += StopGrabAnimation;

                GameManager.instance.VRTKSDKManager.transform.Find("SDKSetups").transform.Find("[CameraRig]").transform.Find("Controller (right)").GetComponent<VRTK_InteractGrab>().GrabButtonPressed += StartGrabAnimation;
                GameManager.instance.VRTKSDKManager.transform.Find("SDKSetups").transform.Find("[CameraRig]").transform.Find("Controller (right)").GetComponent<VRTK_InteractGrab>().GrabButtonReleased += StopGrabAnimation;

                playerY = transform.position.y - 0.2f;


                GameManager.instance.VRTKSDKManager.transform.Find("SDKSetups").transform.Find("[CameraRig]").transform.Find("Controller (left)").GetComponent<VRTK_InteractTouch>().ControllerStartUntouchInteractableObject += UntouchedObject;
                GameManager.instance.VRTKSDKManager.transform.Find("SDKSetups").transform.Find("[CameraRig]").transform.Find("Controller (right)").GetComponent<VRTK_InteractTouch>().ControllerStartUntouchInteractableObject += UntouchedObject;

                crossGreen = Resources.Load<Material>("Materials/GreenCross");
                crossRed = Resources.Load<Material>("Materials/RedCross");

                GameManager.instance.playArea.GetComponent<VRTK_BasicTeleport>().Teleported += StartFixFireManPosition;
                GameManager.instance.VRTKSDKManager.transform.Find("SDKSetups").transform.Find("[CameraRig]").transform.Find("Controller (left)").transform.Find("hand_1").transform.Find("Pip Boy").transform.Find("Group4").transform.Find("Mesh4").GetComponent<HandScreen>().StartHandScreen();*/
            }
            //clickHere = true;
            //transform.Find("Controller (left)").transform.Find("hand_1").transform.Find("Pip Boy").GetComponent<AudioSource>().Play();
        }

    }

    public void Setup(string playerName, string playerCharacter)
    {
        // -Give the player an name.
        SetPlayerName();

        //Add this player to the Gamemanager libary of players
        if (!GameManager.instance.players.ContainsKey((int)netId.Value))
        {
            GameManager.instance.players.Add((int)netId.Value, gameObject);
            //player.CmdSetCharacter();

            // Add this player to the score dictionary
            //GameManager.instance.scoreManager.AddPlayer((int)player.netId.Value);
        }
        //TODO: what? player might get added multiple times
        else
        {
            // Add this player to the score dictionary
            //GameManager.instance.scoreManager.AddPlayer((int)player.netId.Value);
        }

        // -If this player is the local player.
        if (isLocalPlayer)
        {
            // -Assign it to the gamemanager as local player.
            //GameManager.instance.localPlayer = player.gameObject;

            // -idk what his is
            //this seems to be the start of a pause menu
            //GameObject worldCanvas = GameObject.Instantiate(Resources.Load<GameObject>("WorldCanvas"), player.transform.Find("Camera (eye)"));
            //player.pauseMenu = worldCanvas.transform.GetChild(0).Find("PauseMenu").gameObject;

            // -Disable the graphics of the head so we can see.
            DisableGameobjects();
        }
        // -if its not local player
        else
        {
            // -Disable all the components we dont want.
            DisableComponents(playerName, playerCharacter);
        }
    }

    public void DisableComponents(string playerName, string playerCharacter)
    {
        //transform.Find("[CameraRig]").transform.Find("Canvas").transform.Find("Text").GetComponent<Text>().text = playerName;
        //transform.Find("[CameraRig]").transform.Find("Canvas").transform.Find("Text (1)").gameObject.SetActive(false);
        //transform.Find("[CameraRig]").transform.Find(playerCharacter).gameObject.SetActive(true);
        transform.GetChild(0).gameObject.SetActive(true);
        // -Disable all the components that the client who is is not this player dont need.
        foreach (Behaviour _behaviour in componentsToDisable)
        {
            _behaviour.enabled = false;
        }
        foreach (AudioListener _audioListner in GetComponentsInChildren<AudioListener>())
        {
            _audioListner.enabled = false;
        }
    }

    // -Give the player an name basid on his netID.
    private void SetPlayerName()
    {
        name += "(" + netId + ")";
    }

    //Disable all the models on the players head.
    private void DisableGameobjects()
    {
        //Transform eyes = player.transform.Find("Camera (eye)").Find("Head");
        //eyes.gameObject.SetActive(false);
        //eyes.Find("Eye_Right").gameObject.SetActive(false);
        //eyes.Find("Eye_Left").gameObject.SetActive(false);
        /*foreach (GameObject item in characters)
        {
            foreach (Transform tr in item.transform)
            {
                tr.gameObject.layer = 13;
            }

        }*/
        foreach (GameObject _gameObject in gameObjectsToDisable)
        {
            _gameObject.SetActive(false);
        }
    }
}

